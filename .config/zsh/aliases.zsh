#!/usr/bin/env bash

#Easier disk reading
alias df='df -h'                          
alias free='free -m'                      

#ls/exa
alias ls='exa -al --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'
alias ll='exa -l --color=always --group-directories-first'
alias lt='exa -aT --color=always --group-directories-first'
alias l.='exa -a | egrep "^\."'

#Git
alias addup='git add -u'
alias addall='git add .'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias stat='git status'  
alias tag='git tag'
alias newtag='git tag -a'

#Colorized grep
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

#Permissions
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

#NeoVim
alias vim='nvim'



